;
(function($){
	$(document).on('change', '.js-fileChoose', function(){
		var $parent = $(this).parent();
		var $fileNameNode = $parent.find('.file-name');
		var $that = $(this);
		
		var regexFileName = /.*(.*)$/;
		
		if(!$that.data('filename_original'))
		{
			$that.data('filename_original', $fileNameNode.html());
		}
		
		var fileName = $that.data('filename_original');
		
		if(this.value.length > 0)
		{
			if(('files' in this) && this.files.length > 0)
			{
				var filesArray = [];
				
				$.each(this.files, function(){
					filesArray.push(this.name);
				});
				
				fileName = filesArray.join(', ');
			}
			else
			{
				fileName = this.value.replace(regexFileName, "$1").replace(regexFileName, "$1");
			}
		}
		
		fileName = $('<div/>').text(fileName).html();
		
		$parent.find('.file-name').attr('title', fileName).html(fileName);
	});
	
	var jsCarousel = $('.js-carousel');
	if(jsCarousel.length > 0)
	{
		jsCarousel.each(function(){
				
				var $that = $(this);
				var $items = $that.data('carousel-items') || 4;
				var options = {};
				options.initial = {
					items     :$items,
					singleItem:parseInt($items) === 1,
					autoHeight:$that.data('carousel-autoheight') || false,
					margin    :$that.data('carousel-margin') || 30,
					nav       :$that.data('carousel-nav') === true,
					dots      :$that.data('carousel-dots') === true,
					center    :$that.data('carousel-center') === true,
					loop      :$that.data('carousel-loop') === true,
				};
				options.xs = {
					items :$that.data('carousel-xs-items') || options.initial.items,
					loop  :$that.data('carousel-xs-loop') || options.initial.loop,
					margin:$that.data('carousel-xs-margin') || options.initial.margin,
					center:$that.data('carousel-xs-center') || options.initial.center,
					nav   :$that.data('carousel-xs-nav') || options.initial.nav,
					dots  :$that.data('carousel-xs-dots') || options.initial.dots,
				};
				options.sm = {
					items :$that.data('carousel-sm-items') || options.initial.items,
					loop  :$that.data('carousel-sm-loop') || options.initial.loop,
					margin:$that.data('carousel-sm-margin') || options.initial.margin,
					center:$that.data('carousel-sm-center') || options.initial.center,
					nav   :$that.data('carousel-sm-nav') || options.initial.nav,
					dots  :$that.data('carousel-sm-dots') || options.initial.dots,
				};
				options.md = {
					items :$that.data('carousel-md-items') || options.initial.items,
					loop  :$that.data('carousel-md-loop') || options.initial.loop,
					margin:$that.data('carousel-md-margin') || options.initial.margin,
					center:$that.data('carousel-md-center') || options.initial.center,
					nav   :$that.data('carousel-md-nav') || options.initial.nav,
					dots  :$that.data('carousel-md-dots') || options.initial.dots,
				};
				options.lg = {
					items :$that.data('carousel-lg-items') || options.initial.items,
					loop  :$that.data('carousel-lg-loop') || options.initial.loop,
					margin:$that.data('carousel-lg-margin') || options.initial.margin,
					center:$that.data('carousel-lg-center') || options.initial.center,
					nav   :$that.data('carousel-lg-nav') || options.initial.nav,
					dots  :$that.data('carousel-lg-dots') || options.initial.dots,
				};
				options.xs.singleItem = (options.xs.items === 1);
				options.sm.singleItem = (options.sm.items === 1);
				options.md.singleItem = (options.md.items === 1);
				options.lg.singleItem = (options.lg.items === 1);
				$that.owlCarousel({
					items     :options.initial.items,
					singleItem:options.initial.singleItem,
					loop      :options.initial.loop,
					margin    :options.initial.margin,
					center    :options.initial.center,
					nav       :options.initial.nav,
					dots      :options.initial.dots,
					navText   :["<i class=''><</i>", "<i class='ico ico_arrow-double-right'>></i>"],
					responsive:{
						0  :options.xs,
						300:options.sm,
						600:options.md,
						993:options.lg
					}
				});
			}
		);
	}
	
	window.sidebar = new Slideout({
		panel    :document.getElementById('slideout-panel'),
		menu     :document.getElementById('slideout-menu'),
		padding  :window.outerWidth - 64,
		side     :'right',
		tolerance:70,
		touch    :false
	});
	$('.js-sidebar-toggle').on('click', function(){
		window.sidebar.toggle();
	});
	
	var jsDropdown = $('.js-dropdown');
	console.log(jsDropdown);
	if(jsDropdown.length > 0)
	{
		jsDropdown.each(function(){
			$(this).on('click', '.js-dropdown__menu li', function(){
				$(this).parents('.js-dropdown').children('.js-dropdown__button').text($(this).text());
			});
		});
	}
})
(jQuery);
