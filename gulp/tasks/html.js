var browserSync, gulp, reload, rigger, handleErrors;
gulp = require('gulp');
rigger = require('gulp-rigger');
browserSync = require('browser-sync');
reload = browserSync.reload;
handleErrors = require('../util/handleErrors');

gulp.task('html', function(){
	return gulp.src('./src/html/[^_]**/*.html')
		.on('error', handleErrors)
		.pipe(rigger())
		.pipe(gulp.dest('./build'))
		.pipe(reload({
			stream:true
		}));
});
