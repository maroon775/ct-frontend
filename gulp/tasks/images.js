var changed, gulp, handleErrors;
gulp         = require('gulp');
changed      = require('gulp-changed');
handleErrors = require('../util/handleErrors');

gulp.task('images', function ()
{
	return gulp.src([
		'./src/images/**/*.*',
		'!./src/images/sprite/**/*.*'
	]).pipe(changed('./build/i')).on('error', handleErrors).pipe(gulp.dest('./build/i'));
});
