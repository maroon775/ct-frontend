var gulp, handleErrors, uglify;
gulp         = require('gulp');
uglify       = require('gulp-uglify');
handleErrors = require('../util/handleErrors');

gulp.task('bower', function ()
{
	var arJs;
	arJs = [];
	arJs.push('./bower_components/jquery/dist/jquery.min.js');
	arJs.push('./bower_components/owl.carousel/dist/owl.carousel.min.js');
	arJs.push('./bower_components/slideout.js/dist/slideout.min.js');
	arJs.push('./bower_components/slideout.js/dist/slideout.js');
	
	gulp.src([
		'./bower_components/owl.carousel/dist/assets/owl.carousel.min.css',
		'./bower_components/owl.carousel/dist/assets/owl.theme.default.min.css'
	]).pipe(gulp.dest('./build/css/'));
	
	return gulp.src(arJs).pipe(gulp.dest('./src/js/'));
});
