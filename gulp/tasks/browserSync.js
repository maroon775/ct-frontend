var browserSync, gulp;
gulp        = require('gulp');
browserSync = require('browser-sync');

gulp.task('browserSync', function ()
{
	return browserSync({
		reloadOnRestart: true,
		port           : 9000,
//		reloadThrottle : 500,
		open           : false,
		server         : {
			baseDir: ['./build']
		},
		files          : ['./build/**']
	});
});
